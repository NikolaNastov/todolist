<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Models\Task;
use App\Http\Controllers\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'],function(){
    Route::get('/dashboard', [TaskController::class,'showTasks'])->name('dashboard');
    Route::view('profile','profile')->name('profile');
    Route::put('profile',[ProfileController::class, 'update'])->name('profile.update');
});


Route::get('/create',[TaskController::class,'createTask'])->name('create');

Route::post('/create',[TaskController::class,'storeTask'])->name('store');

Route::get('/edit/{task}',[TaskController::class, 'editTask'])->name('edit');

Route::post('/edit/{task}',[TaskController::class,'updateTask'])->name('update');

Route::post('/delete',[TaskController::class, 'deleteTask'])->name('delete');

Route::post('/moveUp',[TaskController::class,'moveUp'])->name('moveUp');

Route::post('/moveDown',[TaskController::class,'moveDown'])->name('moveDown');

require __DIR__.'/auth.php';
