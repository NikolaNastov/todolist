<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Task') }}
        </h2>
    </x-slot>
    <script>

        function testfunt(){
            //document.getElementById("tagcheck").checked == true
            if(document.getElementById("tagcheck").checked == true){
                document.getElementById("tagtext").style.display = "inline";
                document.getElementById("tagdrop").style.display = "none";
            }else{
                document.getElementById("tagtext").style.display = "none";
                document.getElementById("tagdrop").style.display = "inline";
            }
            
        }
        
    </script>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="" method="POST">
                        @csrf
                        <label for="">Name of task</label></br>
                        <input type="text" name="taskname" id=""></br>
                        <label for="">Tag</label></br>
                        <input id="tagtext" name="tagtext" type="text" style="display: none;">
                        <select id="tagdrop" name="tagdrop" id="" style="width: 229px">
                            @foreach($tags as $tag)
                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                            @endforeach 
                        </select>
                        <input id="tagcheck" type="checkbox" name="tagcheck" value="true" onclick="testfunt()" unchecked><label for="">new tag</label>
                        </br>
                        <label for="">Due date</label></br>
                        <input type="text" name="duedate" placeholder='2021-07-22'></br>
                        <label for="">Due time</label></br>
                        <input type="text" name="duetime" placeholder='17:35'></br></br>
                        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Add Task</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        testfunt();
    </script>
</x-app-layout>
