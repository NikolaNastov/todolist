<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function showActive(){
            document.getElementById("active").style.display = "block";
            document.getElementById("archived").style.display = "none";
        }
        function showArchived(){
            document.getElementById("active").style.display = "none";
            document.getElementById("archived").style.display = "block";
        }
    </script>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div id="active" class="p-6 bg-white border-b border-gray-200 space-x-5">
                    <script>
                        function btclick(form){
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You won't be able to revert this!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Yes, delete it!'
                            }).then((result) => {
                                if(result.isConfirmed){
                                    form.submit()
                                }
                            });
                            return false;
                        }
                            
                    </script>
                    <a href="{{route('create')}}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline">create new task</a>
                    <button onclick="showActive()" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline">Show active</button>
                    <button onclick="showArchived()" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline">Show archived</button>
                    </br>
                    <div class="inline-block w-1/4"><h2><b>To do:</b></h2></div>
                    <div class="inline-block w-1/3"><h2><b>In progress:</b></h2></div>
                    <div class="inline-block w-1/4"><h2><b>Done:</b></h2></div>
                    <div class="inline-block w-1/4">
                        
                        @foreach($todoTasks as $task)
                            @php
                            $warndate = date_sub(date_create($task->due_date), date_interval_create_from_date_string('2 days'));
                            $diff = date_diff(date_create(),$warndate);
                            $sign = $diff->format("%R");
                            $warning = false;
                            if($sign == '-'){
                                $class = 'bg-gray-100 mt-6 border-2 border-red-500';
                                $warning = true;
                            }else{
                                $class = 'bg-gray-100 mt-6';
                            }     
                            @endphp
                            <div class="{{$class}}">
                            {{$task->name}}</br>
                            {{$task->tag->name}}</br>
                            <form action="/moveUp" method="POST" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Move to in progress">
                        </form>
                        <form action="/edit/{{$task->id}}" class="inline-block">
                            
                            <input type="submit" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="edit">
                        </form>
                        <form action="/delete" method="POST" onsubmit="return btclick(this);" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Delete">
                        </form>
                        @if($warning == true)
                            <p style="color:red;">Warning: Task due in less than 2 days</p>
                        @endif
                        </div> 
                        @endforeach
                    </div>
                    <div class="inline-block w-1/3">
                        
                         @foreach($progressTasks as $task)
                            @php
                                $warndate = date_sub(date_create($task->due_date), date_interval_create_from_date_string('2 days'));
                                $diff = date_diff(date_create(),$warndate);
                                $sign = $diff->format("%R");
                                $warning = false;
                                if($sign == '-'){
                                     $class = 'bg-gray-100 mt-6 border-2 border-red-500';
                                     $warning = true;
                                }else{
                                    $class = 'bg-gray-100 mt-6';
                            }     
                            @endphp
                        <div class="{{$class}}">
                        {{$task->name}}</br>
                        {{$task->tag->name}}</br>
                        <form action="/moveUp" method="POST" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Move to done">
                        </form>
                        <form action="/moveDown" method="POST" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Move to to do">
                        </form>
                        <form action="/edit/{{$task->id}}" class="inline-block">
                            
                            <input type="submit" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="edit">
                        </form>
                        <form action="/delete" method="POST" onsubmit="return btclick(this);" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Delete">
                        </form>
                        @if($warning == true)
                            <p style="color:red;">Warning: Task due in less than 2 days</p>
                        @endif
                        </div>
                        
                        
                        
                    @endforeach
                    </div>
                    <div class="inline-block w-1/4">
                    
                    @foreach($doneTasks as $task)
                        <div class="bg-gray-100 mt-6">
                        {{$task->name}}</br>
                        {{$task->tag->name}}</br>
                        <form action="/moveDown" method="POST" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Move to in progress">
                        </form>
                        <form action="/edit/{{$task->id}}" class="inline-block">
                            
                            <input type="submit" class="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="edit">
                        </form>
                        <form action="/delete" method="POST" onsubmit="return btclick(this);" class="inline-block">
                            @csrf
                            <input type="text" name="id" value="{{$task->id}}" hidden>
                            <input type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline" value="Delete">
                        </form>
                        </div>
                    @endforeach
                    </div>
                    
                </div>
                <div id="archived" class="p-6 bg-white border-b border-gray-200 space-x-5" style="display:none;">
                    <a href="{{route('create')}}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline">create new task</a>
                    <button onclick="showActive()" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline">Show active</button>
                    <button onclick="showArchived()" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline">Show archived</button>
                    <table>
                        <th>Name</th>
                        <th>Tag</th>
                        <th>creted at</th>
                        <th>completed</th>
                        @foreach($archived as $archive)
                        <tr>
                            <td>{{$archive->name}}</td>
                            <td>{{$archive->tag->name}}</td>
                            <td>{{$archive->created_at}}</td>
                            <td>{{$archive->completed}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
