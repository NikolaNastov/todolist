<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->smallInteger('status');
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("tag_id");
            $table->dateTime("due_date");
            $table->dateTime("completed")->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            #$table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
