<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\tag;
use App\Models\archivedTask;
use Auth;

class TaskController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }
    function showTasks(){
        $todoTasks = Task::where('user_id',Auth::id())->where('status',0)->get();
        $progressTasks = Task::where('user_id',Auth::id())->where('status',1)->get();
        $doneTasks = Task::where('user_id',Auth::id())->where('status',2)->get();
        $archived = archivedTask::where('user_id',Auth::id())->get();
        $this->checkArchive($doneTasks);
        return view('dashboard',['todoTasks'=>$todoTasks, 'progressTasks'=>$progressTasks, 'doneTasks'=>$doneTasks,'archived'=>$archived]);
    }

    function createTask(){
        $tags = tag::all();
        return view('createTask',['tags'=>$tags]);
    }

    function storeTask(){
        if(request('tagcheck') == 'true'){
            $newtag = tag::create(['name' => request('tagtext')]);
            Task::create([
                'name' => request('taskname'),
                'status' => 0,
                'user_id' => Auth::id(),
                'tag_id' => $newtag->id,
                'due_date' => request('duedate') . ' ' . request('duetime')//'1998-01-23 12:45:56'
            ]);
        }else{
            Task::create([
                'name' => request('taskname'),
                'status' => 0,
                'user_id' => Auth::id(),
                'tag_id' => request('tagdrop'),
                'due_date' => request('duedate') . ' ' . request('duetime')//'1998-01-23 12:45:56'
            ]);
        } 
        return redirect('/dashboard');
    }
    function editTask(Task $task){
        $tags = tag::all();
        return view('editTask',['task'=>$task,'tags'=>$tags]);
    }
    function updateTask(Task $task){
        if(request('tagcheck') == 'true'){
            $newtag = tag::create(['name' => request('tagtext')]);
            $task->name = request('taskname');
            $task->tag_id = $newtag->id;
            $task->due_date = request('duedate') . ' ' . request('duetime');
        }else{
            $task->name = request('taskname');
            $task->tag_id = request('tagdrop');
            $task->due_date = request('duedate') . ' ' . request('duetime');
        }
        
        $task->save();
        return redirect('/dashboard');
    }
    function deleteTask(){
        $task = Task::where('id', request('id'));
        $task->delete();
        return redirect('/dashboard');
    }
    function moveUp(){
        $task = Task::where('id', request('id'))->first();
        if($task->status == 0){
            $task->status = 1;
        }else if($task->status == 1){
            $task->status = 2;
            $task->completed = date_create();
        }
        $task->save();
        return redirect('/dashboard');
    }
    function moveDown(){
        $task = Task::where('id', request('id'))->first();
        if($task->status == 2){
            $task->status = 1;
            $task->completed = null;
        }else if($task->status == 1){
            $task->status = 0;
        }
        $task->save();
        return redirect('/dashboard');
    }

    public function checkArchive($tasks){
        foreach($tasks as $task){
            $diff = date_diff(date_create($task->completed),date_create());
            $days = $diff->format("%a");
            if($days >= 2){
                archivedTask::create([
                    'name' => $task->name,
                    'user_id' => $task->user_id,
                    'tag_id' => $task->tag_id,
                    'due_date' => $task->due_date,
                    'completed' => $task->completed
                ]);
                $task->delete();
            }
        }
    }
}
