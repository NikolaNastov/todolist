<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable=['name','user_id','tag_id','due_date','status','completed'];
    public function tag()
    {
        return $this->belongsTo(tag::class);
    }
}
